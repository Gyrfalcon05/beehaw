FROM python:3.9-slim

# Avoid tzdata trying to configure itself if we install any packages
ARG DEBIAN_FRONTEND=noninteractive

RUN pip install pipenv && mkdir /code

COPY Pipfile* /code

COPY dev-startup.sh /

RUN chmod +x /dev-startup.sh && cd /code && pipenv install

VOLUME /code

WORKDIR /code