# beehaw

A social network of some kind. We are still discussing the details on [Discord](https://discord.gg/pVrpNvwkvW), but the main ideas are to be:

- A space which emphasizes quality of content over quantity
- A space which supports a broad range of topics that can freely and safely discussed at length and with nuance
- A space which almost anyone should be able to contribute to

The project is built around the Django Python web framework connected to a Postgres database backend. To get started contributing, all you should need installed on your local machine is [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [Docker](https://docs.docker.com/engine/install/), as well as a Gitlab account:

1. Fork this project using the fork button in the upper right 
2. Clone your fork to your local machine (`git clone https://gitlab.com/your-account/your-project.git`)
3. Add this project as an upstream (`git remote add upstream https://gitlab.com/account-deleted/beehaw.git`). This is helpful in pulling changes that are made by others to the project
4. Copy the `.env.example` files, removing the `.example` suffix, or create your own `.env` files for configuration.
5. In the project root directory, run `docker-compose up`. You should see images being pulled and/or built, and then output from the Django server.
6. Navigate to `localhost:8000` in your browser to see the homepage!
7. Create a branch and make changes to the files. The server should automatically reload upon detecting changes
8. Hit `ctrl-c` to shut down your containers. When you know everything is working, you can use `docker-compose up -d` and `docker-compose stop` to start and stop everything while keeping control of your terminal
9. If you need to make changes inside one of the containers while it is running, you can see their names with `docker ps`, and then shell into them with `docker exec -it container bash`
10. If you need to wipe the database for any reason, you can delete it while the containers are down using `docker volume rm beehaw_database` if it has the default name. If not, you should be able to find it in the list of volumes using `docker volume ls`
