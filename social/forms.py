from django import forms
from .models import Post, Comment


class PostForm(forms.ModelForm):
    # Working with BeautifulSoup package
    web_address = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={
            'rows': '1',
            'placeholder': 'http://'
        })
    )

    body = forms.CharField(
        label='',
        required=False,
        widget=forms.Textarea(attrs={
            'rows': '3',
            'placeholder': 'Say something...'
        })
    )

    class Meta:
        model = Post
        fields = ['web_address', 'body']


class CommentForm(forms.ModelForm):
    comment = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'rows': '3',
            'placeholder': 'Say something...'
        })
    )

    class Meta:
        model = Comment
        fields = ['comment']


class ThreadForm(forms.Form):
    username = forms.CharField(label='', max_length=100)


class MessageForm(forms.Form):
    message = forms.CharField(label='', max_length=1000)


class ShareForm(forms.Form):
    body = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'rows': '3',
            'placeholder': 'Say something...'
            }))


class ExploreForm(forms.Form):
    query = forms.CharField(
        label='',
        widget=forms.TextInput(attrs={
            'placeholder': 'Explore tags'
        })
    )
